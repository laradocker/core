#!/bin/bash

export HEAD=$(git rev-parse HEAD)
export OWNER=john.miller
export REPO=laratest
export TOKEN=ZYScvPUHygxuJN8VrjJy
export ENDPOINT=gitlab.com

docker build -t registry.gitlab.com/john.miller/laratest/nginx:latest \
  --build-arg HEAD=$HEAD \
  --build-arg OWNER=$OWNER \
  --build-arg REPO=$REPO \
  --build-arg TOKEN=$TOKEN \
  --build-arg ENDPOINT=$ENDPOINT \
  -f docker/nginx/Dockerfile docker/nginx

docker build -t registry.gitlab.com/john.miller/laratest/php:latest \
  --build-arg HEAD=$HEAD \
  --build-arg OWNER=$OWNER \
  --build-arg REPO=$REPO \
  --build-arg TOKEN=$TOKEN \
  --build-arg ENDPOINT=$ENDPOINT \
  -f docker/php/Dockerfile docker/php
